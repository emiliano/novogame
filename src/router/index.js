import Vue from "vue";
import VueRouter  from "vue-router";

import Authentication from '../components/Authentication';
import Register from '../components/Register';
import StartGame from '../components/StartGame';
import NovoSite from '../components/NovoSite';

import SaleComplete from '../components/SaleComplete';
import ShopDetails from '../components/ShopDetails';
import Shop from '../components/Shop';
import CorrectResult from '../components/CorrectResult';
import FailResult from '../components/FailResult';
import NovoGame from '../components/NovoGame';
import Score from '../components/Score';
import SuccessRegister from '../components/SuccessRegister';

import dataStore from '../store';

Vue.use(VueRouter);

let router =  new VueRouter({
  // mode: 'history',
  mode: 'hash',
  routes:[
    {
      path: '/',
      name: 'home',
      component: NovoSite
    },
    {
      path: '/start',
      name: 'start',
      component: StartGame,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/quiz',
      name: 'quiz',
      component: NovoGame,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/shop',
      name: 'shop',
      component: Shop,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/shopdetails',
      name: 'shopdetails',
      component: ShopDetails,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/salecomplete',
      name: 'salecomplete',
      component: SaleComplete,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/correct',
      name: 'correct',
      component: CorrectResult,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/fail',
      name: 'fail',
      component: FailResult,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/score',
      name: 'score',
      component: Score,
      meta: {
        requiresAuth: true
      }
    },
    { 
      path: '/login', 
      name: 'login',
      component: Authentication 
    },
    { 
      path: '/register', 
      name: 'register',
      component: Register 
    },
    { 
      path: '/successregister', 
      name: 'successregister',
      component: SuccessRegister 
    },
    { 
      path: '*', 
      redirect: '/' 
    },
  ]
});

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (dataStore.state.auth.authenticated) {
      next();
      return;
    }
    next('/');
  } else {
    next();
  }
});

export default router;