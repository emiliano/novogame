import {apiUrl} from '../env';

export default {
  state: {
    products: [],
    productdetail: [],
    product: {},
    order: {},
  },
  mutations: {
    setProducts(state, data) {
      state.products = data;
    },
    setProduct(state, product){
      state.product = product;
    },
    setProductDetails(state, data){
      state.productdetail = data;
    },
  },
  getters:{
  },
  actions:{
    async getProducts(context){
      let response = (
        await context.getters.authenticatedAxios.get(`${apiUrl}/product`)
      ).data;
      context.commit('setProducts', response.data)
    },
    selectedProduct(context, product){
      context.commit('setProduct', product);
    },
    async getProductDetails(context, productdetails){
        let response = (
          await context.getters.authenticatedAxios.get(`${apiUrl}/product/${productdetails}`)
        ).data;
        context.commit('setProductDetails', response);
    },
    async saleProductAccept(context, saleproduct){
      console.info({saleproduct});
      let response = await context.getters.authenticatedAxios.post(`${apiUrl}/shoppingcart`, saleproduct);
      console.info({response});

      return response.status === 201 ? true : false;
    },

  }
}