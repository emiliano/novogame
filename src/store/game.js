import {apiUrl} from '../env';
import Axios from 'axios';

export default {
  // namespaced:true,
  state: {
    quiz: {},
    categories: [],
    categorySlctd: {},
    options: ['A', 'B', 'C', 'D'],
    wallet:{},
    timeForResponse: 15,
    timeFinished: false,
    tryAgain: false,
    showAgain: false,
    showFoqui: true,
    topten: []
  },
  mutations: {
    setInitialGame(state){
      state.timeForResponse = 15;
      state.showFoqui = true;
    },
    setCategories(state, data) {
      state.categories = data;
      state.timeForResponse = 15;
    },
    setCategorySlctd(state, cat){
      state.categorySlctd = cat
    },
    setQuiz(state, quiz){
      state.quiz = quiz
    },
    setUserWallet(state, wallet){
      state.wallet = wallet
    },
    setTimeFinished(state, finished){
      state.timeFinished = finished
    },
    setTryAgain(state, stts){
      state.tryAgain = stts
    },
    setShowAgain(state, stts){
      state.showAgain = stts
    },
    setTopTenUsers(state, topten){
      state.topten = topten
    },
    setShowAssistant(state, show){
      state.showFoqui = show
    }
  },
  getters:{ },
  actions:{
    async getQuiz(context, idUser){
      const {tryAgain} = this.state.game;
      const idCategory = this.state.game.categorySlctd.id;
      if(!tryAgain){
        context.commit('setQuiz', {});  
        let response = (
          await context.getters.authenticatedAxios.get(`${apiUrl}/quiz/${idCategory}/${idUser}`)
        ).data;
        context.commit('setQuiz', response[0]);  
      }
    },
    async retrieveCategories(context){
      console.info({context});
      let response = (
        await context.getters.authenticatedAxios.get(`${apiUrl}/category`)
      ).data;
      context.commit('setCategories', response.data);
    },
    selectedCategory(context, cat){
      context.commit('setCategorySlctd', cat);
    },
    async getWallet(context, id){
      let response = await context.getters.authenticatedAxios.get(`${apiUrl}/wallet/${id}`);
      context.commit('setUserWallet', response.data[0]);
    },
    async updateCronws(context, wallet){
      let response = (
        await context.getters.authenticatedAxios.put(`${apiUrl}/wallet/${wallet.id}`, wallet)
      ).data;
      context.commit('setUserWallet', response);
    },
    updateTimeFinished(context, stts){
      context.commit('setTimeFinished', stts);
    },
    updateTryAgain(context, stts){
      context.commit('setTryAgain', stts);
    },
    updateShowAgain(context, stts){
      context.commit('setShowAgain', stts);
    },
    updateShowAssitant(context, stts){
      context.commit('setShowAssistant', stts);
    },
    initialGame(context){
      context.commit('setInitialGame');
    },
    async getTopUsers(context, limit){
      let response = (
        await Axios.get(`${apiUrl}/top/${limit}`)
      ).data;
      context.commit('setTopTenUsers', response);
    },
  }
}