  
import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from 'vuex-persistedstate';

import GameModule from "./game";
import AuthModule from "./auth";
import ShopModule from "./shop";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  modules: { 
    game: GameModule, 
    auth: AuthModule, 
    shop: ShopModule, 
  },
  state: { 
    isLoading: false
  },
  plugins: [
    createPersistedState({
      paths: [ 'auth', 'game']
    })
  ],
  getters: { },
  mutations: { 
    setLoading(state, isLoading){
      state.isLoading = isLoading
    },
  },
  actions: { 
    setLoadingState(context, isLoading){
      context.commit('setLoading', isLoading);
    }
  }
});