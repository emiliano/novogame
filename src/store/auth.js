import Axios from 'axios';
import {apiUrl} from '../env';

export default {
  // namespaced:true,
  state:{
    authenticated: localStorage.getItem('token') ? true : false,
    jwt: localStorage.getItem('token') || '',
    identifiedUser : {}
  },
  getters: {
    authenticatedAxios(state){
      return Axios.create({
        headers: {
          'Authorization': `Bearer ${state.jwt}`
        }
      });
    }
  },
  mutations: {
    setAuthenticated(state, data){
      state.jwt = data.access_token;
      state.authenticated = data.access_token ? true : false;
      state.identifiedUser = data.user;
    },
    clearAuthentication(state){
      state.authenticated = false;
      state.jwt = null;
      state.identifiedUser = {};
      
      // state.showFoqui = false;
    },
  },
  actions: {
    authenticate(context, credentials){
      return Axios.post(`${apiUrl}/login`, credentials).then(d => {
        console.log(d);
        if(d.data?.access_token){
          localStorage.setItem('token', d.data.access_token);
          context.commit('setAuthenticated', d.data);
        }
      }).catch(err => {
        console.log("ERRRR:: ", err.response);
        const data = {
          access_token: "",
          jwt: null,
          identifiedUser: { }
        }
        context.commit('setAuthenticated', data);
      });
      // console.log({response});
      // if(response.data?.access_token){
      //   localStorage.setItem('token', response.data.access_token);
      //   context.commit('setAuthenticated', response.data);
      // }else{
      //   const data = {
      //     access_token: "",
      //     jwt: null,
      //     identifiedUser: { }
      //   }
      //   context.commit('setAuthenticated', data);
      // }
    },
    async addUser(context, user) {
      console.log({user});
      let response = await Axios.post(`${apiUrl}/signup`, user, { 
        headers: {
          'Content-Type': 'application/json', 
          'Accept': 'application/json', 
          'X-Requested-With': 'XMLHttpRequest', 
        }
      });
      return response.status === 201 ? true : false;
    },
    logout(context){
      context.commit('clearAuthentication');
    }
  }
}