INSERT INTO answers (quiz_id, content, isCorrect) 
SELECT 
	quiz_id, option_a AS content, 0 as isCorrect 
FROM csvanswers 
UNION ALL 
SELECT 
	quiz_id, option_b AS content, 0 as isCorrect 
FROM csvanswers 
UNION ALL 
SELECT 
	quiz_id, option_c AS content, 0 as isCorrect 
FROM csvanswers 
UNION ALL 
SELECT 
	quiz_id, option_d AS content, 0 as isCorrect 
FROM csvanswers