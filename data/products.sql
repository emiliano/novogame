INSERT INTO products VALUES
    (1,'TNN JERSEY 2021 - GSG','Team Novo Nordisk official Jersey 2021. Short sleeve jersey with full zipper. Interpower material front, UVA and UVB perforated',10,70,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (2,'TNN BIB SHORTS 2021 - GSG','Team Novo Nordisk official BiB shorts 2021. Soft feel elastic mesh on the front and polypropylene mesh on the back.',10,70,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (3,'PREORDER - TNN LS JERSEY 2021','Team Novo Nordisk LS jersey 2021 from GSG in light polyester fleece fabric.',10,77,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (4,'TNN HEAD BAND 2021','TNN 2021 Headband from GSG.',10,20,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (5,'TNN SHOE COVER 2021','TNN 2021 shoecover from GSG. With zip opening.',10,46,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (6,'TNN SPORTS FACEMASK','This product is not possible to return due to Covid19 restrictions.',10,9,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (7,'TNN BIKE CAP 2021','TNN 2021 classic cycling cap from GSG.',10,20,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (8,'TNN BIKE SOCKS','The official Team Novo Nordisk sock from the italian brand BEE1. Top quality cycle-sock in coolmax quality.',10,20,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (9,'TNN ARM SLEEVES','TNN 2018 armwarmers from GSG. Anatomical design. Silicone tape holds the sleeves in place.',10,33,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (10,'TNN SUMMER GLOVES','TNN summer gloves from GSG with L.S.A.T. absorption system.',10,26,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (11,'TNN LEGWARMERS','TNN legwarmers from GSG. Anatomical design. Silicone tape holds the sleeves in place.',10,37,'2020-12-29 21:21:45','2020-12-29 21:21:45'),
    (12,'TNN WINTER GLOVES','TNN vinter gloves from GSG.',10,33,'2020-12-29 21:21:45','2020-12-29 21:21:45');
